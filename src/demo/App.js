import React from 'react';
import { Header, Navi } from '../lib';

const App = () => (
  <div>
    <Navi />
  </div>
);

export default App;
