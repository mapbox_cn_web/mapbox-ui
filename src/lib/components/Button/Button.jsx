import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import SvgIcon from 'components/SvgIcon';
import styles from './Button.module.scss';

class Button extends PureComponent {
  render() {
    const {
      className,
      theme,
      label,
      additionalLabel,
      icon,
      size,
      position,
      textTransform,
      disabled,
      onClick,
      type
    } = this.props;

    return (
      <button
        className={`${styles.root} ${styles[`theme_${theme}`]} ${
          styles[`textTransform_${textTransform}`]
        } ${disabled ? styles.disabled : ''} ${className}`}
        type={type}
        disabled={disabled ? 'disabled' : ''}
        onClick={onClick}
      >
        {position === 'left' &&
          icon && (
            <SvgIcon
              className={styles.iconLeft}
              name={icon}
              width={size}
              height={size}
            />
          )}
        <span>{label}</span>
        {additionalLabel && (
          <div className={styles.additionalLabel}>{additionalLabel}</div>
        )}
        {position === 'right' &&
          icon && (
            <SvgIcon
              className={styles.iconRight}
              name={icon}
              width={size}
              height={size}
            />
          )}
      </button>
    );
  }
}

Button.propTypes = {
  className: PropTypes.string,
  theme: PropTypes.oneOf([
    'default',
    'purple',
    'gradient',
    'gradientInvert',
    'light',
    'transparent',
    'borderLight',
    'borderBlue',
    'borderGray',
    'orange',
    'lightBlue',
    'lightPink',
    'roundWhite',
    'success',
    'error',
    'disabled'
  ]),
  label: PropTypes.string,
  additionalLabel: PropTypes.string,
  icon: PropTypes.string,
  size: PropTypes.number,
  position: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  textTransform: PropTypes.oneOf(['default', 'uppercase']),
  type: PropTypes.string
};

Button.defaultProps = {
  className: '',
  theme: 'default',
  label: '',
  icon: null,
  size: 24,
  position: 'right',
  type: 'button',
  disabled: false,
  textTransform: 'default',
  onClick: () => {}
};

export default Button;
