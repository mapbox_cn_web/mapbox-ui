import React from 'react'
import PropTypes from 'prop-types'
// import { Link } from 'gatsby'

import Text from 'components/Text'
import SvgIcon from 'components/SvgIcon'
import styles from './ButtonLink.module.scss'

class ButtonLink extends React.Component {
  render() {
    const { className, link, children, hover } = this.props

    return (
      <a href={link} className={`${styles.root} ${className}`}>
        <Text
          className={(styles.link, hover ? styles.hover : '')}
          tag='span'
          fontSize='small'
          fontWeight='semibold'
          textAlign='center'
          color='emphasis'
        >
          {children}
          <SvgIcon className={styles.icon} name='chevronRight' height='14px' />
        </Text>
      </a>
    )
  }
}

ButtonLink.propTypes = {
  className: PropTypes.string,
  link: PropTypes.string,
  children: PropTypes.string,
  hover: PropTypes.bool
}

export default ButtonLink
