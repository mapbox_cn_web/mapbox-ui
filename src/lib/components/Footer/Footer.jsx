import React from 'react'
import styles from './Footer.module.scss'
// import img from './arrow-left.png'

class Header extends React.PureComponent {
  state = {
    image: null
  }

  render() {
    import('./arrow-left.png').then(image => {
      this.setState({ image: image.default })
    })

    return (
      <div className={styles.root}>
        <img src={this.state.image} />
      </div>
    )
  }
}

export default Header
