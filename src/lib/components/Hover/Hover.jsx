import React, { PureComponent } from 'react'
import debounce from 'lodash/debounce'
import PropTypes from 'prop-types'

class Hover extends PureComponent {
  handleWapperEvent = event => {
    this.handleEvent({ type: event.type, event })
  }

  handleEvent = debounce(({ type, event }) => {
    const { onHover } = this.props

    switch (type) {
      case 'mouseenter':
        onHover(true, event)
        break
      case 'mouseleave':
        onHover(false, event)
        break
      default:
    }
  }, this.props.debounce)

  render() {
    const { className, children } = this.props

    return (
      <div
        className={className}
        onMouseEnter={this.handleWapperEvent}
        onMouseLeave={this.handleWapperEvent}
      >
        {children}
      </div>
    )
  }
}

Hover.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
  onHover: PropTypes.func,
  debounce: PropTypes.number,
}

Hover.defaultProps = {
  onHover: () => {},
  debounce: 200,
}

export default Hover
