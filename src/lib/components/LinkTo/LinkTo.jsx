import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { PopupContext } from 'context'

import parse from 'url-parse'

class LinkTo extends PureComponent {
  componentDidMount() {
    this.siteHost = window.location.host
  }

  handleAway = (onAway, href) => event => {
    event.preventDefault()
    event.stopPropagation()

    onAway(href)
  }

  handlePreventDefault = href => event => {
    event.preventDefault()
    event.stopPropagation()

    window.open(href, '_self')
  }

  render() {
    const { className, href, children } = this.props
    const { host } = parse(href)

    if (host === 'www.mapbox.com') {
      return (
        <PopupContext.Consumer>
          {({ onAway }) => (
            <a
              className={className}
              href={href}
              onClick={this.handleAway(onAway, href)}
            >
              {children}
            </a>
          )}
        </PopupContext.Consumer>
      )
    }

    if (host === this.siteHost) {
      return (
        <a
          className={className}
          href={href}
          onClick={this.handlePreventDefault(href)}
        >
          {children}
        </a>
      )
    }

    return (
      <a
        className={className}
        href={href}
        onClick={this.handlePreventDefault(href)}
      >
        {children}
      </a>
    )
  }
}

LinkTo.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any
}

export default LinkTo
