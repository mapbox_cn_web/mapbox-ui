import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'

import IconSvg from 'components/SvgIcon'
import styles from './PopoverComunity.module.scss'

export default class Item extends PureComponent {
  static propTypes = {
    icon: PropTypes.string,
    name: PropTypes.string,
    desc: PropTypes.string,
    href: PropTypes.string,
    sizeIcon: PropTypes.number,
    sizeCircle: PropTypes.number
  }

  static defaultProps = {
    sizeCircle: 42,
    sizeIcon: 20
  }

  render() {
    const { icon, name, desc, href, sizeIcon, sizeCircle } = this.props

    return (
      <div className={styles.item}>
        <a href={href}>
          <div
            style={{ width: `${sizeCircle}px`, height: `${sizeCircle}px` }}
            className={styles.icon}
          >
            <IconSvg width={sizeIcon} height={sizeIcon} name={icon} />
          </div>
          <div className={styles.content}>
            <span>{name}</span>
            <p>{desc}</p>
          </div>
        </a>
      </div>
    )
  }
}
