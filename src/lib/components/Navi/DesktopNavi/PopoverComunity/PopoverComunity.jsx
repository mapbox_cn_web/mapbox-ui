import React, { PureComponent, Fragment } from 'react'
import styles from './PopoverComunity.module.scss'

import PropTypes from 'prop-types'

import Item from './Item'
import Popover from 'components/Popover'

export default class PopoverComunity extends PureComponent {
  render() {
    const { data } = this.props

    return (
      <Popover
        style={{ marginLeft: 12 }}
        styleWrapper={{
          display: 'flex',
          flexFlow: 'column',
          width: 215,
          height: 184,
          justifyContent: 'center'
        }}
      >
        <Item
          href={data.blog.href}
          icon={data.blog.icon}
          name={data.blog.name}
          desc={data.blog.desc}
        />
        <Item
          href={data.activity.href}
          icon={data.activity.icon}
          name={data.activity.name}
          desc={data.activity.desc}
        />
      </Popover>
    )
  }
}
