import React, { PureComponent } from 'react'
// import { Link, push } from 'gatsby'
import styles from './PopoverDocumentation.module.scss'

import PropTypes from 'prop-types'

import Text from 'components/Text'
import Popover from 'components/Popover'
import ButtonLink from 'components/ButtonLink'
import SideItem from './SideItem'

export default class PopoverDocumentation extends PureComponent {
  handleClick = data => event => {
    if (data.target) {
      window.open(data.href)
    } else {
      // push(data.href)
    }
  }

  render() {
    const { data } = this.props

    return (
      <Popover
        style={{ marginLeft: 25 }}
        styleWrapper={{ width: 744, height: 322 }}
        className={styles.root}
      >
        <div className={`row ${styles.row}`}>
          <div className='col-11'>
            <div className='row'>
              <div className='col-4'>
                <div className={styles.headingTitle}>{data.heading1}</div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.androidMapSdk)}
                >
                  {data.androidMapSdk.name}
                </div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.androidNavigationSDK)}
                >
                  {data.androidNavigationSDK.name}
                </div>
              </div>
              <div className='col-4'>
                <div className={styles.headingTitle}>{data.heading2}</div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.iOSMapSDK)}
                >
                  {data.iOSMapSDK.name}
                </div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.iOSNavigationSDK)}
                >
                  {data.iOSNavigationSDK.name}
                </div>
              </div>
              <div className='col-4'>
                <div className={styles.headingTitle}>{data.heading3}</div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.mapGLJS)}
                >
                  {data.mapGLJS.name}
                </div>
              </div>
              <div className='col-4'>
                <div className={styles.headingTitle}>{data.heading4}</div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.unitySDK)}
                >
                  {data.unitySDK.name}
                </div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.qtSDK)}
                >
                  {data.qtSDK.name}
                </div>
              </div>
              <div className='col-4'>
                <div className={styles.headingTitle}>{data.heading5}</div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.apiMaps)}
                >
                  {data.apiMaps.name}
                </div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.apiDirections)}
                >
                  {data.apiDirections.name}
                </div>
                <div
                  className={styles.navLink}
                  onClick={this.handleClick(data.apiGeocoding)}
                >
                  {data.apiGeocoding.name}
                </div>
              </div>
            </div>
            <div className={styles.allDocs}>
              <div
                className={styles.navMore}
                onClick={this.handleClick(data.allDocumentation)}
              >
                <ButtonLink className={styles.more} link='/documentation'>
                  {data.allDocumentation.name}
                </ButtonLink>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.side}>
          <Text color='light' fontSize='mini'>
            辅助类文档和工具
          </Text>
          <div className={styles.line} />
          <SideItem
            className={styles.item}
            link='/examples'
            title='示例中心'
            desc='通过浏览示例以及程序代码了解如何使用Mapbox'
          />
          <SideItem
            className={styles.item}
            link='https://www.mapbox.com/help/interactive-tools/'
            title='地图工具'
            desc='一些帮助程序员开发Mapbox地图的小工具'
          />
        </div>
      </Popover>
    )
  }
}
