import React, { PureComponent } from 'react'
import IconSvg from 'components/SvgIcon'
import styles from './PopoverProducts.module.scss'

import PropTypes from 'prop-types'

import Item from './Item'
import Popover from 'components/Popover'

export default class PopoverProducts extends PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired
  }

  getStyle = image => {

  }

  render() {
    const { data } = this.props

    return (
      <Popover
        style={{ marginLeft: 25 }}
        styleWrapper={{ width: 400 }} // 439 new
        className={styles.root}
      >
        <div className={styles.section1}>
          <div className={styles.headingTitle}>{data.heading1}</div>
          <div className='row'>
            <div className={`col-6 ${styles.column}`}>
              <Item
                href={data.maps.href}
                icon={data.maps.icon}
                name={data.maps.name}
                desc={data.maps.desc}
              />
            </div>
            <div className={`col-6 ${styles.column}`}>
              <Item
                href={data.navigation.href}
                icon={data.navigation.icon}
                name={data.navigation.name}
                desc={data.navigation.desc}
              />
            </div>
            <div className={`col-6 ${styles.column}`}>
              <Item
                href={data.search.href}
                icon={data.search.icon}
                name={data.search.name}
                desc={data.search.desc}
              />
            </div>
            <div className={`col-6 ${styles.column}`}>
              <Item
                href={data.studio.href}
                icon={data.studio.icon}
                name={data.studio.name}
                desc={data.studio.desc}
              />
            </div>
          </div>
          <div className={`${styles.headingTitle} ${styles.second}`}>
            {data.heading2}
          </div>
          <div className='row justify-content-between'>
            {
              <div className={`col-3 ${styles.column}`}>
                <Item
                  href={data.web.href}
                  icon={data.web.icon}
                  name={data.web.name}
                  sizeCircle={30}
                  sizeIcon={16}
                />
              </div>
            }
            <div className={`col-3 ${styles.column}`}>
              <Item
                href={data.mobile.href}
                icon={data.mobile.icon}
                name={data.mobile.name}
                sizeCircle={30}
                sizeIcon={17}
              />
            </div>
            <div className={`col-3 ${styles.column}`}>
              <Item
                href={data.AR.href}
                icon={data.AR.icon}
                name={data.AR.name}
                sizeCircle={30}
                sizeIcon={16}
              />
            </div>
            <div className={`col-3 ${styles.column}`}>
              <Item
                href={data.auto.href}
                icon={data.auto.icon}
                name={data.auto.name}
                sizeCircle={30}
                sizeIcon={19}
              />
            </div>
          </div>
        </div>
      </Popover>
    )
  }
}
