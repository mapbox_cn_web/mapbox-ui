import React, { PureComponent, Fragment } from 'react'

import styles from './PopoverTutorials.module.scss'

import PropTypes from 'prop-types'

import Text from 'components/Text'
import Image from 'components/Image'
import Popover from 'components/Popover'

export default class PopoverTutorials extends PureComponent {
  render() {
    return (
      <Popover
        style={{ marginLeft: 12, marginTop: 33 }}
        styleWrapper={{ paddingTop: 33, width: 190, height: 201 }}
      >
        <a href='/tutorials/android' className={styles.item}>
          <div className={styles.circle}>
            <Image width={15} path='navi/android.svg' />
          </div>
          <Text fontWeight='semibold' fontSize='mini'>
            安卓 SDK 基础
          </Text>
        </a>
        <a href='/tutorials/ios' className={styles.item}>
          <div className={styles.circle}>
            <Image width={15} path='navi/apple.svg' />
          </div>
          <Text fontWeight='semibold' fontSize='mini'>
            iOS SDK 基础
          </Text>
        </a>
        <a href='/tutorials/gljs' className={styles.item}>
          <div className={styles.circle}>
            <Image width={20} path='navi/comp.svg' />
          </div>
          <Text fontWeight='semibold' fontSize='mini'>
            GL JS 基础
          </Text>
        </a>
      </Popover>
    )
  }
}
