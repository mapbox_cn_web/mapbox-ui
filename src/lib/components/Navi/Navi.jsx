import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { mobileWidth } from 'data/breakpoints';
import Logo from 'components/Logo';
import DesktopNavi from './DesktopNavi';
import TabletNavi from './TabletNavi';
import Burger from './Burger';

import styles from './Navi.module.scss';

// import 'animate.css/animate.css'
// import 'font-awesome/css/font-awesome.css'

const mobileWidthCheck =
  typeof window !== 'undefined' && window.matchMedia
    ? window.matchMedia(`(max-width: ${mobileWidth}px)`)
    : {};

export default class Navi extends PureComponent {
  state = {
    status: false
  };

  static propTypes = {
    isTransparent: PropTypes.bool
  };

  static defaultProps = {
    isTransparent: false
  };

  state = {
    isMobile: typeof window !== 'undefined' ? window.innerWidth <= 768 : false,
    status: false,
    resolution:
      typeof window !== 'undefined' &&
      window.innerWidth &&
      window.innerWidth <= mobileWidth
        ? 'mobile'
        : 'desktop'
  };

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
    mobileWidthCheck.addListener(this.setMobileLogo);
  }

  // -- был баг с меню -- //
  //
  // componentDidUpdate() {
  //   const { status } = this.state
  //
  //   document.body.style.overflow = status ? 'hidden' : null
  //   document.documentElement.style.overflow = status ? 'hidden' : null
  // }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
    mobileWidthCheck.removeListener(this.setMobileLogo);
  }

  setMobileLogo = mediaQueryList => {
    const { matches } = mediaQueryList;

    this.setState({ resolution: matches ? 'mobile' : 'desktop' });
  };

  handleResize = () => {
    if (!this.mobile && window.innerWidth <= 769) {
      this.mobile = true;
      this.setState({ isMobile: true });
    } else if (this.mobile && window.innerWidth > 769) {
      this.mobile = false;
      this.setState({ isMobile: false });
    }
  };

  handleClick = () => {
    const { status } = this.state;

    this.setState({ status: !status });

    document.documentElement.style.overflow = !status ? 'hidden' : null;
  };

  render() {
    const { isTransparent } = this.props;

    const rootClass = !isTransparent
      ? styles.root
      : `${styles.root} ${styles.transparent}`;
    const { isMobile, status, resolution } = this.state;

    return (
      <nav>
        <div className="container-fluid">
          <div className={rootClass}>
            <div className={styles.wrapper}>
              <Logo
                width={158}
                type={resolution}
                withTitle
                isTransparent={status === true ? false : isTransparent}
              />
              {isMobile && (
                <Burger status={status} onClick={this.handleClick} />
              )}
              {!isMobile && (
                <DesktopNavi isTransparent={isTransparent} pathname="" />
              )}
            </div>
          </div>
          <TabletNavi isTransparent={isTransparent} isVisible={status} />
        </div>
      </nav>
    );
  }
}
