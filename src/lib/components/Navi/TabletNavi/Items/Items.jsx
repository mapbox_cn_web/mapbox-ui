import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styles from './Items.module.scss'

export default class Items extends PureComponent {
  static propTypes = {
    items: PropTypes.array,
    title: PropTypes.string
  }

  handleClick = () => {
    document.documentElement.style.overflow = null
  }

  render() {
    const { title, items } = this.props

    return (
      <div className={styles.root}>
        <div className={styles.title}>{title}</div>
        <div className={styles.items}>
          {items.map(({ title, href }, i) => (
            <a href={href} key={i} onClick={this.handleClick}>
              <div className={styles.item}>{title}</div>
            </a>
          ))}
        </div>
      </div>
    )
  }
}
