import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import sprites from './sprite'

export default class Icon extends PureComponent {
  static propTypes = {
    name: PropTypes.string.isRequired,
    small: PropTypes.bool,
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      .isRequired,
    className: PropTypes.string,
    grayScale: PropTypes.bool,
  }

  static defaultProps = {
    small: false,
    width: 'auto',
    height: 'auto',
    className: '',
    grayScale: false,
  }

  render() {
    const { name, small, width, height, className, grayScale } = this.props
    const CSSClasses = ['icon', `icon_${name}`]

    if (!sprites[name]) {
      return <div style={{ color: 'red' }}>{name}</div>
    }
    const { svg, viewBox } = sprites[name]

    if (className) {
      CSSClasses.push(className)
    }

    if (small) {
      CSSClasses.push('icon_small')
    }

    return (
      <svg
        className={CSSClasses.join(' ')}
        style={{ width, height }}
        viewBox={viewBox}
      >
        {svg}
      </svg>
    )
  }
}
