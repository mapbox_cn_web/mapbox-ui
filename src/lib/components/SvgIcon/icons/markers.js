import React, { Fragment } from 'react'

const markers = {
  markerSchool: {
    viewBox: '0 0 107 107',
    svg: (
      <Fragment>
        <defs>
          <circle id="path-school1" cx="39.5" cy="39.5" r="39.5" />
          <filter
            x="-29.1%"
            y="-24.1%"
            width="158.2%"
            height="158.2%"
            filterUnits="objectBoundingBox"
            id="filter-school2"
          >
            <feOffset
              dx="0"
              dy="4"
              in="SourceAlpha"
              result="shadowOffsetOuter1"
            />
            <feGaussianBlur
              stdDeviation="7"
              in="shadowOffsetOuter1"
              result="shadowBlurOuter1"
            />
            <feColorMatrix
              values="0 0 0 0 0.258823529   0 0 0 0 0.388235294   0 0 0 0 0.984313725  0 0 0 0.5 0"
              type="matrix"
              in="shadowBlurOuter1"
            />
          </filter>
        </defs>
        <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g transform="translate(-534.000000, -1554.000000)">
            <g transform="translate(-581.000000, 1110.000000)">
              <g transform="translate(580.000000, 0.000000)">
                <g transform="translate(549.000000, 454.000000)">
                  <g>
                    <use
                      fill="black"
                      fillOpacity="1"
                      filter="url(#filter-school2)"
                      xlinkHref="#path-school1"
                    />
                    <use
                      fill="#4263FB"
                      fillRule="evenodd"
                      xlinkHref="#path-school1"
                    />
                  </g>
                  <g
                    transform="translate(22.000000, 24.000000)"
                    fill="#FFFFFF"
                    fillRule="nonzero"
                  >
                    <path
                      d="M18,0 L0,8.34615385 L4.8,10.4923077 L4.8,14.5461538 C3.36,15.0230769 2.4,16.4538462 2.4,17.8846154 C2.4,19.3153846 3.36,20.7461538 4.8,21.2230769 L4.8,21.4615385 L2.64,26.4692308 C1.92,28.6153846 2.4,31 6,31 C9.6,31 10.08,28.6153846 9.36,26.4692308 L7.2,21.4615385 C8.64,20.7461538 9.6,19.5538462 9.6,17.8846154 C9.6,16.2153846 8.64,15.0230769 7.2,14.5461538 L7.2,11.6846154 L18,16.6923077 L36,8.34615385 L18,0 Z M28.56,15.5 L17.76,20.2692308 L12,17.6461538 L12,17.8846154 C12,19.5538462 11.28,20.9846154 10.08,22.1769231 L11.52,25.5153846 L11.52,25.7538462 C11.76,26.7076923 12,27.6615385 11.76,28.6153846 C13.44,29.3307692 15.36,29.8076923 17.76,29.8076923 C25.68,29.8076923 28.56,25.0384615 28.56,22.6538462 L28.56,15.5 Z"
                      id="Shape"
                    />
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </Fragment>
    ),
  },
  markerBar: {
    viewBox: '0 0 120 120',
    svg: (
      <Fragment>
        <defs>
          <circle id="path-bar1" cx="46" cy="46" r="46" />
          <filter
            x="-25.0%"
            y="-20.7%"
            width="150.0%"
            height="150.0%"
            filterUnits="objectBoundingBox"
            id="filter-bar2"
          >
            <feOffset
              dx="0"
              dy="4"
              in="SourceAlpha"
              result="shadowOffsetOuter1"
            />
            <feGaussianBlur
              stdDeviation="7"
              in="shadowOffsetOuter1"
              result="shadowBlurOuter1"
            />
            <feColorMatrix
              values="0 0 0 0 0.196078431   0 0 0 0 0.764705882   0 0 0 0 0.466666667  0 0 0 0.5 0"
              type="matrix"
              in="shadowBlurOuter1"
            />
          </filter>
        </defs>
        <g
          id="Page-1"
          stroke="none"
          strokeWidth="1"
          fill="none"
          fillRule="evenodd"
        >
          <g
            id="A03_Home_DropDown"
            transform="translate(-860.000000, -1244.000000)"
          >
            <g id="advantages" transform="translate(-581.000000, 1110.000000)">
              <g id="advantages-01" transform="translate(580.000000, 0.000000)">
                <g id="bar-icon" transform="translate(875.000000, 144.000000)">
                  <g id="Oval">
                    <use
                      fill="black"
                      fillOpacity="1"
                      filter="url(#filter-bar2)"
                      xlinkHref="#path-bar1"
                    />
                    <use
                      fill="#32C377"
                      fillRule="evenodd"
                      xlinkHref="#path-bar1"
                    />
                  </g>
                  <g
                    id="bars"
                    transform="translate(30.000000, 27.000000)"
                    fill="#FFFFFF"
                    fillRule="nonzero"
                  >
                    <path
                      d="M12.6541353,6.96428571 L12.6541353,5.57142857 C13.430656,5.57142857 14.0601504,4.94782519 14.0601504,4.17857143 C14.0601504,3.40931767 13.430656,2.78571429 12.6541353,2.78571429 L12.6541353,1.39285714 C12.6541353,0.623603384 12.024641,4.71032077e-17 11.2481203,0 L8.43609023,0 C7.65956956,-4.71032077e-17 7.03007519,0.623603384 7.03007519,1.39285714 L7.03007519,2.78571429 C6.25355452,2.78571429 5.62406015,3.40931767 5.62406015,4.17857143 C5.62406015,4.94782519 6.25355452,5.57142857 7.03007519,5.57142857 L7.03007519,6.96428571 C7.03007519,10.9478571 0,15.5164286 0,19.5 L0,36.2142857 C0,37.7527932 1.25898875,39 2.81203008,39 L16.8721805,39 C18.3677124,38.870454 19.5534407,37.6958221 19.6842105,36.2142857 L19.6842105,19.5 C19.6842105,15.7392857 12.6541353,10.725 12.6541353,6.96428571 Z M9.82778947,33.6818182 C6.96463158,33.6818182 4.47368421,30.5050909 4.47368421,26.5909091 C4.47368421,22.6767273 6.96463158,19.5 9.82778947,19.5 C12.6909474,19.5 15.2105263,22.6767273 15.2105263,26.5909091 C15.2105263,30.5050909 12.6909474,33.6818182 9.82778947,33.6818182 Z M34,11.5227273 L22.3684211,11.5227273 L22.3684211,20.9749091 C22.3684211,20.9749091 22.3684211,20.9749091 22.3684211,21.1397727 C22.3770098,23.6390464 24.169276,25.8180778 26.7302632,26.4428864 L26.7302632,36.2522727 L25.2763158,36.2522727 C24.4733228,36.2522727 23.8223684,36.8673724 23.8223684,37.6261364 C23.8223684,38.3849003 24.4733228,39 25.2763158,39 L31.0921053,39 C31.8950982,39 32.5460526,38.3849003 32.5460526,37.6261364 C32.5460526,36.8673724 31.8950982,36.2522727 31.0921053,36.2522727 L29.6381579,36.2522727 L29.6381579,26.4428864 C32.1991451,25.8180778 33.9914113,23.6390464 34,21.1397727 C34,21.1397727 34,21.1397727 34,20.9749091 L34,11.5227273 Z M31.3157895,21.1461039 C31.3157895,22.6846114 30.1140275,23.9318182 28.6315789,23.9318182 C27.1491304,23.9318182 25.9473684,22.6846114 25.9473684,21.1461039 L25.9473684,14.1818182 L31.3157895,14.1818182 L31.3157895,21.1461039 Z"
                      id="Shape"
                    />
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </Fragment>
    ),
  },
  markerPool: {
    viewBox: '0 0 68 68',
    svg: (
      <Fragment>
        <defs>
          <circle id="path-pool1" cx="20" cy="20" r="20" />
          <filter
            x="-57.5%"
            y="-47.5%"
            width="215.0%"
            height="215.0%"
            filterUnits="objectBoundingBox"
            id="filter-pool2"
          >
            <feOffset
              dx="0"
              dy="4"
              in="SourceAlpha"
              result="shadowOffsetOuter1"
            />
            <feGaussianBlur
              stdDeviation="7"
              in="shadowOffsetOuter1"
              result="shadowBlurOuter1"
            />
            <feColorMatrix
              values="0 0 0 0 0.2   0 0 0 0 0.764705882   0 0 0 0 0.466666667  0 0 0 0.5 0"
              type="matrix"
              in="shadowBlurOuter1"
            />
          </filter>
        </defs>
        <g
          id="Page-1"
          stroke="none"
          strokeWidth="1"
          fill="none"
          fillRule="evenodd"
        >
          <g
            id="A03_Home_DropDown"
            transform="translate(-667.000000, -1366.000000)"
          >
            <g id="advantages" transform="translate(-581.000000, 1110.000000)">
              <g id="advantages-01" transform="translate(580.000000, 0.000000)">
                <g id="pool-icon" transform="translate(682.000000, 266.000000)">
                  <g id="Oval">
                    <use
                      fill="black"
                      fillOpacity="1"
                      filter="url(#filter-pool2)"
                      xlinkHref="#path-pool1"
                    />
                    <use
                      fill="#33C377"
                      fillRule="evenodd"
                      xlinkHref="#path-pool1"
                    />
                  </g>
                  <g
                    id="pool"
                    transform="translate(11.000000, 13.000000)"
                    fill="#FFFFFF"
                    fillRule="nonzero"
                  >
                    <path
                      d="M12.8076834,0 C12.6652206,0 12.255991,0.186434168 12.255991,0.186434168 L8.04778693,2.32421874 C7.48742627,2.54848091 7.26531247,3.44671702 7.6024738,3.89524135 L8.8320322,5.68749959 L3.8024738,8.27024103 L6.33333333,10.1793318 L9.5024738,8.27024103 L12.6691405,10.1793318 L13.9382822,8.90411772 L10.1382822,3.813209 L13.3766929,1.86683241 C14.0462782,1.52805407 13.9382822,0.970170657 13.9382822,0.631391045 C13.9332155,0.364187064 13.4820187,5.65204408e-16 12.8076834,0 Z M15.5191405,3.81818154 C14.2939457,3.81818154 13.3,4.81439856 13.3,6.04545411 C13.3,7.27651093 14.2939445,8.27521231 15.5191405,8.27521231 C16.7443377,8.27521231 17.7358071,7.27651093 17.7358071,6.04545411 C17.7358071,4.81439856 16.7443377,3.81818154 15.5191405,3.81818154 Z M3.16666667,10.1818172 L0,12.090908 L0,14 L3.16666667,12.090908 L6.33333333,14 L9.5024738,12.090908 L12.6691405,14 L15.2,12.090908 L19,14 L19,12.090908 L15.2,10.1818172 L12.6691405,12.090908 L9.5024738,10.1818172 L6.33333333,12.090908 L3.16666667,10.1818172 Z"
                      id="Shape"
                    />
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </Fragment>
    ),
  },
  markerAq: {
    viewBox: '0 0 68 68',
    svg: (
      <Fragment>
        <defs>
          <circle id="path-aq1" cx="20" cy="20" r="20" />
          <filter
            x="-57.5%"
            y="-47.5%"
            width="215.0%"
            height="215.0%"
            filterUnits="objectBoundingBox"
            id="filter-aq2"
          >
            <feOffset
              dx="0"
              dy="4"
              in="SourceAlpha"
              result="shadowOffsetOuter1"
            />
            <feGaussianBlur
              stdDeviation="7"
              in="shadowOffsetOuter1"
              result="shadowBlurOuter1"
            />
            <feColorMatrix
              values="0 0 0 0 0.850980392   0 0 0 0 0.847058824   0 0 0 0 0.219607843  0 0 0 0.5 0"
              type="matrix"
              in="shadowBlurOuter1"
            />
          </filter>
        </defs>
        <g
          id="Page-1"
          stroke="none"
          strokeWidth="1"
          fill="none"
          fillRule="evenodd"
        >
          <g
            id="A03_Home_DropDown"
            transform="translate(-740.000000, -1662.000000)"
          >
            <g id="advantages" transform="translate(-581.000000, 1110.000000)">
              <g id="advantages-01" transform="translate(580.000000, 0.000000)">
                <g id="aq-icon" transform="translate(755.000000, 562.000000)">
                  <g id="Oval">
                    <use
                      fill="black"
                      fillOpacity="1"
                      filter="url(#filter-aq2)"
                      xlinkHref="#path-aq1"
                    />
                    <use
                      fill="#D9D838"
                      fillRule="evenodd"
                      xlinkHref="#path-aq1"
                    />
                  </g>
                  <g
                    id="aquarium"
                    transform="translate(12.000000, 13.000000)"
                    fill="#FFFFFF"
                    fillRule="nonzero"
                  >
                    <path
                      d="M11.6266667,10.6512077 C11.3066667,10.0483092 11.3066667,8.34009662 11.6266667,7.83768116 C12.0533333,7.23478261 15.2533333,9.24444444 15.2533333,9.24444444 C16.2133333,9.64637681 16.2133333,3.11497585 15.2533333,3.51690821 C15.2533333,3.51690821 11.9466667,5.62705314 11.6266667,4.9236715 C11.3066667,4.22028986 11.3066667,2.81352657 11.6266667,2.11014493 C11.9466667,1.50724638 16,1.40676329 16,1.40676329 C16,0.703381643 12.9066667,0 11.6266667,0 C10.3466667,0 8.96,0.100483092 7.25333333,0.803864734 C5.54666667,1.40676329 4.16,2.4115942 2.88,3.6173913 C1.6,4.82318841 0,7.23478261 0,7.93816425 C0,8.64154589 1.6,10.7516908 3.94666667,11.6560386 C6.29333333,12.5603865 7.46666667,12.7613527 8.74666667,12.9623188 C9.92,13.0628019 11.52,12.9623188 12.9066667,12.6608696 C13.9733333,12.4599034 16,11.9574879 16,11.5555556 C16,11.3545894 11.9466667,11.2541063 11.6266667,10.6512077 Z M4.8,8.34009662 C3.94666667,8.34009662 3.2,7.63671498 3.2,6.83285024 C3.2,6.02898551 3.94666667,5.32560386 4.8,5.32560386 C5.65333333,5.32560386 6.4,6.02898551 6.4,6.83285024 C6.4,7.63671498 5.65333333,8.34009662 4.8,8.34009662 Z"
                      id="Shape"
                    />
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </Fragment>
    ),
  },
  markerAircraft: {
    viewBox: '0 0 88 88',
    svg: (
      <Fragment>
        <defs>
          <circle id="path-aircraft1" cx="30" cy="30" r="30" />
          <filter
            x="-38.3%"
            y="-31.7%"
            width="176.7%"
            height="176.7%"
            filterUnits="objectBoundingBox"
            id="filter-aircraft2"
          >
            <feOffset
              dx="0"
              dy="4"
              in="SourceAlpha"
              result="shadowOffsetOuter1"
            />
            <feGaussianBlur
              stdDeviation="7"
              in="shadowOffsetOuter1"
              result="shadowBlurOuter1"
            />
            <feColorMatrix
              values="0 0 0 0 0.749019608   0 0 0 0 0.768627451   0 0 0 0 0.792156863  0 0 0 0.5 0"
              type="matrix"
              in="shadowBlurOuter1"
            />
          </filter>
        </defs>
        <g
          id="Page-1"
          stroke="none"
          strokeWidth="1"
          fill="none"
          fillRule="evenodd"
        >
          <g
            id="A01_Home_MVP"
            transform="translate(-1186.000000, -1662.000000)"
          >
            <g id="advantages" transform="translate(-581.000000, 1110.000000)">
              <g id="advantages-01" transform="translate(580.000000, 0.000000)">
                <g
                  id="aq-icon-copy"
                  transform="translate(1201.000000, 562.000000)"
                >
                  <g id="Oval">
                    <use
                      fill="black"
                      fillOpacity="1"
                      filter="url(#filter-aircraft2)"
                      xlinkHref="#path-aircraft1"
                    />
                    <use
                      fill="#BFC4CA"
                      fillRule="evenodd"
                      xlinkHref="#path-aircraft1"
                    />
                  </g>
                  <g
                    id="airport"
                    transform="translate(19.000000, 19.000000)"
                    fill="#FFFFFF"
                    fillRule="nonzero"
                  >
                    <path
                      d="M23,10.4545457 L22.9999985,13.0333333 L13.0333325,11.5 L12.5454547,18.8181829 L16.8666655,21.4666667 L16.8666655,23 L11.4999989,21.9545457 L6.13333292,23 L6.13333292,21.4666667 L10.4545447,18.8181829 L9.966666,11.5 L0,13.0333333 L0,10.4545457 L9.966666,6.9 L9.966666,2.3 C9.966666,2.3 9.966666,0 11.4999989,0 C13.0333325,0 13.0333325,2.3 13.0333325,2.3 L13.0333325,6.62121225 L23,10.4545457 Z"
                      id="Shape"
                    />
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </Fragment>
    ),
  },
}

export default markers
