import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

import styles from './Text.module.scss'

export default class Text extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    tag: PropTypes.string, // Tag that will wrap content
    fontSize: PropTypes.oneOf([
      'mini', // 12
      'small', // 14
      'normal', // 16
      'medium', // 18
      'medium_l', // 20
      'medium_xl', // 24
      'big', // 36
      'large', // 48
      'great', // 64
    ]),
    mobileFontSize: PropTypes.oneOf(['medium']),
    desktopFontSize: PropTypes.oneOf(['big']),
    fontWeight: PropTypes.oneOf(['regular', 'semibold']),
    letterSpacing: PropTypes.oneOf(['standard']),
    color: PropTypes.oneOf([
      'light',
      'lightGray',
      'primary',
      'secondary',
      'tertiary',
      'emphasis',
      'emphasisDark',
    ]),
    textAlign: PropTypes.oneOf(['auto', 'left', 'center', 'right']),
    portraitTextAlign: PropTypes.oneOf(['left', 'center', 'right']),
    lineHeight: PropTypes.oneOf([
      'tiny',
      'small',
      'normal',
      'medium_s',
      'medium',
      'medium_l',
    ]),
    opacity: PropTypes.oneOf(['light', 'middle', 'dark']),
    cursor: PropTypes.oneOf(['auto', 'pointer']),
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    onClick: PropTypes.func,
  }

  static defaultProps = {
    tag: 'p',
    fontSize: 'normal',
    fontWeight: 'regular',
    color: 'primary',
    textAlign: 'left',
  }

  handleClick = () => {
    const { onClick } = this.props

    onClick && onClick()
  }

  render() {
    const {
      className,
      tag,
      fontSize,
      mobileFontSize,
      desktopFontSize,
      fontWeight,
      letterSpacing,
      lineHeight,
      color,
      textAlign,
      portraitTextAlign,
      opacity,
      cursor,
      children,
    } = this.props

    const Tag = tag

    const cn = classnames(className, {
      [styles[`fontSize_${fontSize}`]]: fontSize,
      [styles[`mobileFontSize_${mobileFontSize}`]]: mobileFontSize,
      [styles[`desktopFontSize_${desktopFontSize}`]]: desktopFontSize,
      [styles[`fontWeight_${fontWeight}`]]: fontWeight,
      [styles[`letterSpacing_${letterSpacing}`]]: letterSpacing,
      [styles[`lineHeight_${lineHeight}`]]: lineHeight,
      [styles[`color_${color}`]]: color,
      [styles[`textAlign_${textAlign}`]]: textAlign,
      [styles[`portraitTextAlign_${portraitTextAlign}`]]: portraitTextAlign,
      [styles[`opacity_${opacity}`]]: opacity,
      [styles[`cursor_${cursor}`]]: cursor,
    })

    return (
      <Tag className={cn} onClick={this.handleClick}>
        {children}
      </Tag>
    )
  }
}
