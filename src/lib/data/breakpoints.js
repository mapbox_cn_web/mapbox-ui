export const mobileWidth = 375
export const portraitWidth = 768
export const landscapeWidth = 1024
export const desktopWidth = 1280
export const hdWidth = 1440
export const fullHdWidth = 1920
