export const footerLinks = [
  {
    href: null,
    name: '产品',
    title: '产品',
  },
  {
    href: null,
    name: '平台',
    title: '平台',
  },
  {
    href: null,
    name: '开发者资源',
    title: '开发者资源',
  },
  {
    href: null,
    name: '公司',
    title: '公司',
  },
  {
    href: '/product-maps',
    name: '地图',
    title: '地图',
  },
  {
    href: '/product-mobile',
    name: '移动端',
    title: '移动端',
  },
  {
    href: '/documentation',
    name: '开发文档',
    title: '开发文档',
  },
  {
    href: '/about',
    name: '关于我们',
    title: '关于我们',
  },
  {
    href: '/product-navigation',
    name: '导航',
    title: '导航',
  },
  {
    href: '/product-ar',
    name: 'AR',
    title: 'AR',
  },
  {
    href: '/examples',
    name: '示例',
    title: '示例',
  },
  {
    href: '/community-blog',
    name: '博客',
    title: '博客',
  },
  {
    href: '/product-search',
    name: '定位',
    title: '定位',
  },
  {
    href: '/product-auto',
    name: '汽车平台',
    title: '汽车平台',
  },
  {
    href: '/tutorials',
    name: '教程',
    title: '教程',
  },
  {
    href: '/events',
    name: '活动',
    title: '活动',
  },
  {
    href: '/product-studio',
    name: '工作室',
    title: '工作室',
  },
  {
    href: '/tutorials/gljs',
    name: '网页端',
    title: '网页端',
  },
  {
    name: '',
    title: '',
  },
  {
    href: '/contact-us',
    name: '联系我们',
    title: '联系我们',
  },
]

export const mobileFooterLiks = [
  {
    href: null,
    name: '产品',
    title: '产品',
  },
  {
    href: null,
    name: '平台',
    title: '平台',
  },
  {
    href: '/product-maps',
    name: '地图',
    title: '地图',
  },
  {
    href: '/product-mobile',
    name: '移动端',
    title: '移动端',
  },
  {
    href: '/product-navigation',
    name: '导航',
    title: '导航',
  },
  {
    href: '/product-ar',
    name: 'AR',
    title: 'AR',
  },
  {
    href: '/product-search',
    name: '定位',
    title: '定位',
  },
  {
    href: '/product-auto',
    name: '汽车平台',
    title: '汽车平台',
  },
  {
    href: '/product-studio',
    name: '工作室',
    title: '工作室',
  },
  {
    href: '/tutorials/gljs',
    name: '网页端',
    title: '网页端',
  },
  {
    href: null,
    name: '公司',
    title: '公司',
  },
  {
    href: null,
    name: '开发者资源',
    title: '开发者资源',
  },
  {
    href: '/about',
    name: '关于我们',
    title: '关于我们',
  },
  {
    href: 'documentation',
    name: '开发文档',
    title: '开发文档',
  },
  {
    href: '/community-blog',
    name: '博客',
    title: '博客',
  },
  {
    href: '/examples',
    name: '示例',
    title: '示例',
  },
  {
    href: '/events',
    name: '活动',
    title: '活动',
  },
  {
    href: '/tutorials',
    name: '教程',
    title: '/tutorials',
  },
  {
    href: '/contact-us',
    name: '联系我们',
    title: '联系我们',
  },
]

export const footerCopyrights = {
  privacy: {
    href: '/privacy-policy',
    name: '隐私政策',
    target: '_self',
  },
  mapbox: {
    href: 'https://www.mapbox.com/',
    name: 'mapbox.com',
    target: '_blank',
  },
  address: '沪ICP备16023085号',
}

export const footerSocial = [
  {
    href: 'https://mp.weixin.qq.com/s/11lKI1rMi2oDmnrLITOmyg',
    icon: 'socials/weixin.png',
    name: 'weixin',
  },
  {
    href: 'https://www.zhihu.com/org/mapboxzhong-guo/activities',
    icon: 'socials/zhihu.png',
    name: 'zhihu',
  },
  {
    href: 'https://www.weibo.com/mapbox?refer_flag=1001030101_',
    icon: 'socials/weibo.png',
    name: 'weibo',
  },
  {
    href: 'https://github.com/mapbox',
    icon: 'socials/github.png',
    name: 'github',
  },
  {
    href: 'https://www.linkedin.com/company/mapbox/',
    icon: 'socials/linkedin.png',
    name: 'linkedin',
  },
]
