const navLinks = [
  {
    href: '/products',
    name: '产品介绍',
    title: '产品介绍',
    popover: {
      heading1: '产品介绍',
      heading2: '平台',
      maps: {
        icon: 'navMap',
        href: '/product-maps',
        name: '地图',
        title: '地图',
        desc: '平滑，高速，实时的全球矢量地图',
      },
      navigation: {
        icon: 'navPoint',
        href: '/product-search',
        name: '定位',
        title: '定位',
        desc: '用地址，坐标，地名搜索世界上的地方',
      },
      search: {
        icon: 'navArrow',
        href: '/product-navigation',
        name: '导航',
        title: '导航',
        desc: '实时交通和精准的路线规划体验',
      },
      studio: {
        icon: 'navStudio',
        href: '/product-studio',
        name: '工作室',
        title: '工作室',
        desc: `强大的地图编辑软件\n(即将登陆中国)`,
      },
      web: {
        icon: 'navWeb',
        href: '/tutorials/gljs',
        name: '网页端',
        title: '网页端',
      },
      mobile: {
        icon: 'navMobile',
        href: '/product-mobile',
        name: '移动端',
        title: '移动端',
      },
      AR: {
        icon: 'navAR',
        href: '/product-ar',
        name: '增强现实',
        title: '增强现实',
      },
      auto: {
        icon: 'navAuto',
        href: '/product-auto',
        name: '汽车',
        title: '汽车',
      },
    },
  },
  {
    href: '/documentation',
    name: '开发文档',
    title: '开发文档',
    popover: {
      heading1: 'Android',
      heading2: 'IOS',
      heading3: 'Web端',
      heading4: '其他平台',
      heading5: 'Web 服务',
      androidMapSdk: {
        href: '/sdk/android_sdk',
        name: 'Android 地图 SDK',
      },
      androidNavigationSDK: {
        href: 'https://www.mapbox.com/android-docs/navigation/overview/',
        name: 'Android 导航 SDK',
        target: '_blank',
      },
      iOSMapSDK: {
        href: '/sdk/ios_sdk',
        name: 'iOS 地图 SDK',
      },
      iOSNavigationSDK: {
        href: 'https://www.mapbox.com/mapbox-navigation-ios/navigation/0.18.1/',
        name: 'iOS 导航 SDK',
        target: '_blank',
      },
      mapGLJS: {
        href: 'map-gl-js',
        name: '地图 GL JS',
      },
      unitySDK: {
        href: 'unity-sdk',
        name: 'Unity SDK',
      },
      qtSDK: {
        href: 'https://www.mapbox.com/qt/',
        name: 'Qt SDK',
        target: '_blank',
      },
      apiMaps: {
        href: 'https://www.mapbox.com/api-documentation/#maps',
        name: '地图 API',
        target: '_blank',
      },
      apiDirections: {
        href: 'https://www.mapbox.com/api-documentation/#directions',
        name: '方向 API',
        target: '_blank',
      },
      apiGeocoding: {
        href: 'https://www.mapbox.com/api-documentation/#geocoding',
        name: '定位 API',
        target: '_blank',
      },
      examples: {
        href: '/examples',
        name: 'Examples',
        desk1: '',
        desk2: '',
      },
      interactiveTools: {
        href: 'https://www.mapbox.com/help/interactive-tools/',
        name: 'Interactive Tools',
        target: '_blank',
        desk1: '',
        desk2: '',
      },
      allDocumentation: {
        href: '/documentation',
        name: '全部开发文档',
      },
    },
  },
  // {
  //   href: '/about',
  //   name: '关于我们',
  //   title: '关于我们',
  // },
  {
    href: '/showcases',
    name: '解决方案',
    title: '解决方案',
  },
  {
    href: '/tutorials',
    name: '教程',
    title: '教程',
    popover: {
      blog: {
        icon: 'miniCopy',
        href: '/community-blog',
        name: '安卓 SDK 基础',
      },
      activity: {
        icon: 'miniStar',
        href: '/events',
        name: 'iOS SDK 基础',
      },
      activity: {
        icon: 'miniStar',
        href: '/events',
        name: 'iOS SDK 基础',
      },
    },
  },
  {
    href: '/community',
    name: '社区',
    title: '社区',
    popover: {
      blog: {
        icon: 'miniCopy',
        href: '/community-blog',
        name: '博客',
        desc: '开发者小窍门，案例分析，以及公司新闻',
      },
      activity: {
        icon: 'miniStar',
        href: '/events',
        name: '活动',
        desc: '查看我们或第三方在中国组织的活动',
      },
    },
  },
]

export default navLinks
