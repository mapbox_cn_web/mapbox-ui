import Header from './components/Header';
import Navi from './components/Navi';

import 'scss/bootstrap.scss';
import 'scss/gatstrap.scss';

export { Header, Navi };
